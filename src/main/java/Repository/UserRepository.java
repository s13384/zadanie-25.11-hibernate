package Repository;

import java.util.ArrayList;

import TableModels.User;

public interface UserRepository extends Repository<User>{

	ArrayList<User> withLogin(String login);
	ArrayList<User> withLoginAndPassword(String login, String password);
	void setupPermissions(User user);
	
}
