package Repository;

import java.util.ArrayList;

public interface Repository<TEntity> {

	TEntity withId(int id);
	ArrayList<TEntity> allOnPage(PagingInfo page);
	void add(TEntity entity);
	void delete(TEntity entity);
	void modify(TEntity entity);
	int count();
}
