package Cache;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

import MySQLRepositories.MySQLEnumerationValueRepositoryHibernated;
import Repository.PagingInfo;
import TableModels.EnumerationValue;


public class Updater implements Runnable {

	private static Updater instance;
	private Thread thread = new Thread(this);
	private long lifespan;
	private Cache cacheInstance;
	private ArrayList<EnumerationValue> latestData;
	private MySQLEnumerationValueRepositoryHibernated enumRepo = new MySQLEnumerationValueRepositoryHibernated();
	
	private Updater(){
		
	}
	public static Updater getInstance(){
		if (instance == null)
			instance = new Updater();
		return instance;
	}
	@Override
	public void run() {
		while (true && cacheInstance!=null)
		{
			try {
				consolePrintNewestData();
				refreshCache();
				Thread.sleep(lifespan);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	public void refreshCache(){
		cacheInstance.clean();
		try {
			
			for (EnumerationValue enumValue : enumRepo.allOnPage(new PagingInfo())){
				cacheInstance.put(enumValue);
			}
			
			Thread.sleep(2000);
			cacheInstance.finalizeUpdate();
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}
	public long getLifespan() {
		return lifespan;
	}

	public void setLifespan(long lifespan) {
		this.lifespan = lifespan;
	}

	public Cache getCacheInstance() {
		return cacheInstance;
	}

	public void setCacheInstance(Cache cacheInstance) {
		this.cacheInstance = cacheInstance;
	}
	public ArrayList<EnumerationValue> getLatestData(boolean whichOne) {
		return latestData;
	}
	public void setLatestData(ArrayList<EnumerationValue> latestData) {
		this.latestData = latestData;
	}
	public void start(){
		thread.start();
	}
	public void consolePrintNewestData(){
		if (latestData != null)
		{
		System.out.println(">>Najnowsze dane :");
		for (int i=0; i<latestData.size(); i++){
			System.out.println(">>" + latestData.get(i).getStringKey() + " " + latestData.get(i).getValue() + " " + latestData.get(i).getEnumerationName());
		}
		}
	}

	
	
}