package Cache;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import TableModels.EnumerationValue;

public class Inserter {

	public static void insert(){
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase");
		EntityManager entityManager = entityManagerFactory.createEntityManager();


		entityManager.getTransaction().begin();

		entityManager.persist(new EnumerationValue(1001, "DDOS", "DDoS Attack", "DDOS_TYPE"));
		entityManager.persist(new EnumerationValue(1002, "DDOS", "DDoS Strong Attack", "DDOS_TYPE"));
		entityManager.persist(new EnumerationValue(1003, "DDOS", "DDoS Super Attack", "DDOS_TYPE"));
		entityManager.persist(new EnumerationValue(1004, "DDOS", "DDoS Silent Attack", "DDOS_TYPE"));
		entityManager.persist(new EnumerationValue(2001, "CITY", "Gdansk", "CITY_TYPE"));
		entityManager.persist(new EnumerationValue(2002, "CITY", "Gdynia", "CITY_TYPE"));
		entityManager.persist(new EnumerationValue(2003, "CITY", "Warszawa", "CITY_TYPE"));
		entityManager.persist(new EnumerationValue(2004, "CITY", "Szczecin", "CITY_TYPE"));

		entityManager.getTransaction().commit();

		entityManager.close();
		entityManagerFactory.close();
	}
}
