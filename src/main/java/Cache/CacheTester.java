package Cache;
import java.util.ArrayList;

import TableModels.EnumerationValue;


public class CacheTester implements Runnable{

	Cache instance;
	private Thread thread = new Thread(this);
	private long testTime;
	@Override
	public void run() {
		ArrayList<EnumerationValue> sys = new ArrayList<EnumerationValue>();
		while (true)
		{
		sys = instance.get("DDOS");
		consolePrintResult(sys);
		try {
			Thread.sleep(testTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		}
		
	}
	public void start(Cache instace){
		this.instance=instace;
		thread.start();
	}
	public void consolePrintResult(ArrayList<EnumerationValue> res){
		if(res==null) System.out.println("==Cache Tester BRAK DANYCH");
		if(res!=null){
		System.out.println("==Cache Tester :");
		for (int i=0; i<res.size(); i++){
			System.out.println("==" + res.get(i).getStringKey() + " " + res.get(i).getValue() + " " + res.get(i).getEnumerationName());
		}
		}		
	}
	public void setTestTime(long testTime){
		this.testTime = testTime;
	}

}
