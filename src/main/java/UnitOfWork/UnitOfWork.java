package UnitOfWork;

import TableModels.EntityAbstractClass;

public interface UnitOfWork {

	void saveChanges();
	void undo();
	void markAsNew(EntityAbstractClass entity, UnitOfWorkRepository repo);
	void markAsDeleted(EntityAbstractClass entity, UnitOfWorkRepository repo);
	void markAsChanged(EntityAbstractClass entity, UnitOfWorkRepository repo);
}
