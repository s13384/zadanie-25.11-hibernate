package MySQLRepositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import Repository.EnumerationValueRepository;
import Repository.PagingInfo;
import TableModels.EntityAbstractClass;
import TableModels.EntityState;
import TableModels.EnumerationValue;
import UnitOfWork.UnitOfWorkRepository;
import UnitOfWork.UnitOfWork;

public class MySQLEnumerationValueRepositoryHibernated implements EnumerationValueRepository, UnitOfWorkRepository {

	EntityManagerFactory entityManagerFactory;
	EntityManager entityManager;

	public MySQLEnumerationValueRepositoryHibernated() {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
	}

	@Override
	public EnumerationValue withId(int id) {
		String query = String.format("FROM EnumerationValue WHERE id = %d", id);
		EnumerationValue enumerationValue = (EnumerationValue) entityManager.createQuery(query).getSingleResult();
		return enumerationValue;
	}

	@Override
	public ArrayList<EnumerationValue> allOnPage(PagingInfo page) {
		ArrayList<EnumerationValue> enums = new ArrayList<EnumerationValue>();
		enums = (ArrayList<EnumerationValue>) entityManager.createQuery("FROM EnumerationValue").getResultList();
		return enums;
	}

	@Override
	public void add(EnumerationValue entity) {
		entityManager.persist((EnumerationValue) entity);
	}

	@Override
	public void delete(EnumerationValue entity) {
		entityManager.remove((EnumerationValue) entity);

	}

	@Override
	public void modify(EnumerationValue entity) {
		entityManager.refresh((EnumerationValue) entity);

	}

	@Override
	public int count() {
		return (int) entityManager.createQuery("Select * From").getSingleResult();
	}

	@Override
	public void persistAdd(EntityAbstractClass entity) {
		entityManager.persist((EnumerationValue) entity);
		entityManager.getTransaction().commit();

	}

	@Override
	public void persistDelete(EntityAbstractClass entity) {
		entityManager.remove((EnumerationValue) entity);
		entityManager.getTransaction().commit();

	}

	@Override
	public void persistUpdate(EntityAbstractClass entity) {
		entityManager.refresh((EnumerationValue) entity);
		entityManager.getTransaction().commit();

	}

	@Override
	public ArrayList<EnumerationValue> withName(String name) {
		String query = String.format("FROM EnumerationValue WHERE name = %s", name);
		ArrayList<EnumerationValue> enumerationValue = (ArrayList<EnumerationValue>) entityManager.createQuery(query)
				.getResultList();
		return enumerationValue;
	}

	@Override
	public ArrayList<EnumerationValue> withIntKey(int key, String name) {
		String query = String.format("FROM EnumerationValue WHERE intKey = %d and name = %s", key, name);
		ArrayList<EnumerationValue> enumerationValue = (ArrayList<EnumerationValue>) entityManager.createQuery(query)
				.getResultList();
		return enumerationValue;
	}

	@Override
	public ArrayList<EnumerationValue> withStringKey(String key, String name) {
		String query = String.format("FROM EnumerationValue WHERE  stringKey = key and name = %s", key, name);
		ArrayList<EnumerationValue> enumerationValue = (ArrayList<EnumerationValue>) entityManager.createQuery(query)
				.getResultList();
		return enumerationValue;
	}

}