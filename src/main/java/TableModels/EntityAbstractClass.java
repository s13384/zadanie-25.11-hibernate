package TableModels;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EntityAbstractClass {

	@Id
	@GeneratedValue
	protected int id;
	@Enumerated(EnumType.STRING)
	protected EntityState state;
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	public EntityState getEntityState(){
		return state;
	}
	public void setEntityState(EntityState state){
		this.state = state;
	}
}
