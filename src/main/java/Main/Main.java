package Main;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import Cache.Cache;
import Cache.CacheTester;
import Cache.Inserter;
import Cache.Updater;
import MySQLRepositories.MySQLEnumerationValueRepositoryHibernated;
import TableModels.EntityState;
import TableModels.EnumerationValue;

public class Main {

	public static void main(String[] args) {
		
		try {
			Cache.getInstance();
			Inserter.insert();			
			Updater updater = Updater.getInstance();
			updater.setLifespan(5000);
			updater.setCacheInstance(Cache.getInstance());
			updater.start();
			CacheTester test = new CacheTester();
			test.setTestTime(1000);
			test.start(Cache.getInstance());
			
		}catch (Exception e){
			e.printStackTrace();
		}
		 
	}
	

}
